package com.example.jkhproject.data.repositories;

import java.lang.System;

@dagger.hilt.android.scopes.ViewModelScoped()
@kotlin.Metadata(mv = {1, 5, 1}, k = 1, d1 = {"\u0000\f\n\u0002\u0018\u0002\n\u0002\u0010\u0000\n\u0002\b\u0002\b\u0007\u0018\u00002\u00020\u0001B\u0007\b\u0007\u00a2\u0006\u0002\u0010\u0002\u00a8\u0006\u0003"}, d2 = {"Lcom/example/jkhproject/data/repositories/NewsRepository;", "", "()V", "app_debug"})
public final class NewsRepository {
    
    @javax.inject.Inject()
    public NewsRepository() {
        super();
    }
}