package com.example.jkhproject.data.models.network

data class NewsNetModel(

    val id: String,
    val title: String,
    val description: String,
    val issued: String,
)